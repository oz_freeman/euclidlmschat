const fs = require('fs');
const https = require('https');
const WebSocket = require('ws');

let config = require(__dirname + '/config.json');
if (fs.existsSync(__dirname + '/config-local.json'))
{
    let configLocal = require(__dirname + '/config-local.json');
    Object.keys(configLocal).forEach(function(element)
    {
        config[element] = configLocal[element];
    });
}

const server = new https.createServer(
    {
        cert: fs.readFileSync(config.certificate),
        key: fs.readFileSync(config.key),
        requestCert: false,
        rejectUnauthorized: false
    });

const wss = new WebSocket.Server({ server });

function outLog(message)
{
    //let dateFormat = require('dateformat');
    console.log(/*dateFormat(new Date(), "yyyy-dd-mm hh:MM:ss") + ': ' + */message);
}

wss
    .on('connection', function connection(ws)
    {
        outLog('New connection. ' + wss.clients.size + ' connection(s) on server side');

        ws.isAlive = true;
        ws.userID = null;

        ws
            .on('close', function ()
            {
                outLog('User ' + (ws.userID ? ws.userID : '<guest>') + ' has disconnected. ' + wss.clients.size + ' connection(s) on server side');
            })
            .on('message', function (message)
            {
                message = JSON.parse(message);

                if (!ws.userID && message.type.toString() !== 'login')
                {
                    outLog('Message from unauthorized user. Connection closed');
                    ws.terminate();
                    return 0;
                }

                switch (message.type)
                {
                    case 'login':
                        let credentials = message.credentials;

                        credentials = Buffer.from(credentials, 'base64');
                        if (!credentials || !credentials.length)
                        {
                            outLog('Try to login with incorrect credentials. Connection closed');
                            ws.terminate();
                            return 0;
                        }

                        let crypto = require('crypto');
                        let iv = Buffer.alloc(16);
                        let decryptor = crypto.createDecipheriv(config.decipherivMethod, config.decipherivSecret, iv);
                        decryptor.setAutoPadding(true);
                        try
                        {
                            credentials = decryptor.update(credentials, 'base64', 'utf8');
                            credentials = credentials + decryptor.final('utf8');
                        }
                        catch (ex)
                        {
                            outLog('Decipher failed on a main encode (' + ex.message + '). Connection closed');
                            ws.terminate();
                            return 0;
                        }
                        if (!credentials || !credentials.length)
                        {
                            outLog('Try to login with incorrect credentials. Connection closed');
                            ws.terminate();
                            return 0;
                        }

                        try
                        {
                            credentials = JSON.parse(credentials);
                        }
                        catch (ex)
                        {
                            outLog('Decipher failed on a JSON.parse for crenentials (' + ex.message + '). Connection closed');
                            ws.terminate();
                            return 0;
                        }

                        if (!credentials || typeof credentials !== 'object' || !credentials.hasOwnProperty('id_user'))
                        {
                            outLog('Try to login with incorrect credentials. Connection closed');
                            ws.terminate();
                            return 0;
                        }
                        ws.userID = credentials.id_user;
                        ws.send(JSON.stringify({type: 'login', data: 'ok'}));
                        outLog('User with id=' + ws.userID + ' successful login');
                        break;

                    case 'notify-users-new-message':
                        outLog('User ' + ws.userID + ' send a message to a chat ' + message.id_chat);

                        wss.clients.forEach(function(client)
                        {
                            if (message.users_to_notify.indexOf(client.userID) > -1)
                                client.send(JSON.stringify({type: 'message-added', id_chat: message.id_chat}));
                        });
                        break;

                    case 'notify-message-readed':
                        outLog('User ' + ws.userID + ' read all messages in a chat ' + message.id_chat);

                        wss.clients.forEach(function(client)
                        {
                            if (message.users_to_notify.indexOf(client.userID) > -1)
                                client.send(JSON.stringify({type: 'message-readed', id_chat: message.id_chat}));
                        });
                        break;

                    default:
                        outLog('User ' + ws.userID + ' send an unrecognized command (' + message.type + ')');
                        break;
                }
            })
            .on('error', function()
            {
                outLog('Error user ' + (ws.userID ? ws.userID : '<guest>') + '. Connection closed');
            })
            .on('pong', function()
            {
                ws.isAlive = true;
            });
    });

setInterval(function()
{
    wss.clients.forEach(function (client)
    {
        if (!client.isAlive) return client.terminate();

        client.isAlive = false;
        client.ping(null, false, true);
    });
}, config.checkIsAliveSecondsInterval * 1000);

server.listen(config.port);
outLog('started');